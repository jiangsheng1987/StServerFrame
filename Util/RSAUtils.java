package com.zyl.zylws.utils;

import javax.crypto.Cipher;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

/**
 * RSA算法加密/解密和签名/验签工具类
 * 生成密钥对（公钥和私钥）
 * 加密内容与签名内容进行Base64加密解密（有利于HTTP协议下传输）
 */
public class RSAUtils {
    /**
     * 算法名称
     */
    private static final String ALGORITHM = "RSA";
    /**
     * 签名算法 MD5withRSA 或 SHA1WithRSA 等
     */
    public static final String SIGNATURE_ALGORITHM = "MD5withRSA";
    /**
     * 密钥长度默认是1024位:
     * 加密的明文最大长度 = 密钥长度 - 11（单位是字节，即byte）
     */
    private static final int KEY_SIZE = 1024;
    /**
     * RSA最大加密明文大小
     */
    private static final int MAX_ENCRYPT_BLOCK = 117;

    /**
     * RSA最大解密密文大小
     */
    private static final int MAX_DECRYPT_BLOCK = 128;

    private RSAUtils() {
    }

    /**
     * 获取密钥对
     *
     * @return 密钥对
     */
    public static KeyPair getKeyPair() throws Exception {
        KeyPairGenerator generator = KeyPairGenerator.getInstance(ALGORITHM);
        generator.initialize(KEY_SIZE);
        return generator.generateKeyPair();
    }

    /**
     * 私钥字符串转PrivateKey实例
     *
     * @param privateKey 私钥字符串
     * @return
     */
    public static PrivateKey getPrivateKey(String privateKey) throws Exception {
        KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM);
        byte[] decodedKey = Base64.getDecoder().decode(privateKey.getBytes("UTF-8"));// 对私钥进行Base64编码解密
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(decodedKey);
        return keyFactory.generatePrivate(keySpec);
    }

    /**
     * 公钥字符串转PublicKey实例
     *
     * @param publicKey 公钥字符串
     * @return
     */
    public static PublicKey getPublicKey(String publicKey) throws Exception {
        KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM);
        byte[] decodedKey = Base64.getDecoder().decode(publicKey.getBytes("UTF-8")); // 对公钥进行Base64编码解密
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(decodedKey);
        return keyFactory.generatePublic(keySpec);
    }

    /**
     * 公钥加密
     *
     * @param data      待加密数据
     * @param publicKey 公钥
     * @return
     */
    public static String encryptByPublicKey(String data, PublicKey publicKey) {
        try (
                ByteArrayOutputStream out = new ByteArrayOutputStream();
        ) {
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            int inputLen = data.getBytes("UTF-8").length;
            int offset = 0;
            byte[] cache;
            int i = 0;
            // 对数据分段加密
            while (inputLen - offset > 0) {
                if (inputLen - offset > MAX_ENCRYPT_BLOCK) {
                    cache = cipher.doFinal(data.getBytes("UTF-8"), offset, MAX_ENCRYPT_BLOCK);
                } else {
                    cache = cipher.doFinal(data.getBytes("UTF-8"), offset, inputLen - offset);
                }
                out.write(cache, 0, cache.length);
                i++;
                offset = i * MAX_ENCRYPT_BLOCK;
            }
            byte[] encryptedData = out.toByteArray();
            // 获取加密内容使用Base64进行编码加密,并以UTF-8为标准转化成字符串
            // 加密后的字符串
            //return new String(Base64.encodeBase64String(encryptedData));
            return new String(Base64.getEncoder().encode(encryptedData), "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 私钥解密
     *
     * @param data       待解密数据
     * @param privateKey 私钥
     * @return
     */
    public static String decryptByPrivateKey(String data, PrivateKey privateKey) {
        try (
                ByteArrayOutputStream out = new ByteArrayOutputStream();
        ) {
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, privateKey);

            // 对待解密数据进行Base64编码解密
            byte[] dataBytes = Base64.getDecoder().decode(data.getBytes("UTF-8"));
            int inputLen = dataBytes.length;
            int offset = 0;
            byte[] cache;
            int i = 0;
            // 对数据分段解密
            while (inputLen - offset > 0) {
                if (inputLen - offset > MAX_DECRYPT_BLOCK) {
                    cache = cipher.doFinal(dataBytes, offset, MAX_DECRYPT_BLOCK);
                } else {
                    cache = cipher.doFinal(dataBytes, offset, inputLen - offset);
                }
                out.write(cache, 0, cache.length);
                i++;
                offset = i * MAX_DECRYPT_BLOCK;
            }
            byte[] decryptedData = out.toByteArray();
            // 解密后的内容
            return new String(decryptedData, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 私钥签名
     *
     * @param data       待签名数据
     * @param privateKey 私钥
     * @return 签名
     */
    public static String sign(String data, PrivateKey privateKey) throws Exception {
        byte[] keyBytes = privateKey.getEncoded();
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM);
        PrivateKey key = keyFactory.generatePrivate(keySpec);
        Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
        signature.initSign(key);
        byte [] bdata = data.getBytes(StandardCharsets.UTF_8);
        signature.update(bdata);
        return new String(Base64.getEncoder().encode(signature.sign()));  // 对签名内容进行Base64编码加密
    }

    /**
     * 公钥验签
     *
     * @param srcData   原始字符串
     * @param publicKey 公钥
     * @param sign      签名
     * @return 是否验签通过
     */
    public static boolean verify(String srcData, PublicKey publicKey, String sign) throws Exception {
        byte[] keyBytes = publicKey.getEncoded();
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM);
        PublicKey key = keyFactory.generatePublic(keySpec);
        Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
        signature.initVerify(key);
        signature.update(srcData.getBytes());
        return signature.verify(Base64.getDecoder().decode(sign.getBytes())); // 对验签结果进行Base64编码解密
    }

    public static void main(String[] args) {
        try {
            String src_msg = "realNamezhangsanphone17000000000idCard6403021992120511111bankCard5555555555555555555555";

            // 生成密钥对
            KeyPair keyPair = getKeyPair();
            String privateKey = "MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBAJ3fg0My/zIEbhoi\n" +
                    "Msxm/P1DA25o1UIMxy3f5WhtglrT4Q3Y1us4vdZdw1EN/aJHmLMdV2MoQZV8/x0Y\n" +
                    "88UPev605RiEgJ/mtIcZRzsjKp3/tiD1h9mAHgroHN4U1sJXCPpRoOpfjALVhY4w\n" +
                    "zTm56cZqAPj/rCrT9IrltrMjfjrRAgMBAAECgYAocra3xd4aW4Y2DeMD+bMB1GSR\n" +
                    "qixRYuK5Py8QpVYLnVOA77iIxZ3lyVocb0A3jq5x3aehERRUj5TxSI5UQ9YX+wrp\n" +
                    "e7LZCNGOaNUGMsAOz/MRgNIyK4Ce+byGc8VVj2dA59HPO85nxioJk76BEOwEZ8w5\n" +
                    "vO7/FFK/ogqw37FaLQJBAMyfajHByIpnkSnxmWpKPCP/Aali7eFK17gMTaUtKxtR\n" +
                    "fqIcf3cpm+wO5oqGB6qowv5xXpUeRAfEbb3FSRiRKQsCQQDFgyk+maES3Zk3aYcT\n" +
                    "PLhlXENLqTJ1ipMghWifqTRdgVqtJKevaiNkkI/t/ss/XrFeGqf7pExRTkGkvnZm\n" +
                    "y+0TAkEAzEXkzNFoswrbNyUyg5KiYUkEzqWtVnjRy7G0nc3ut7HumkWa1koal1j0\n" +
                    "u5s78hnRuBNTWchCIALvj0BxTW40qwJBAIEwAyAw+EwQqZC282S/yJRzvoiUiK6d\n" +
                    "DxKsb/xOfjPS0e6CFTQFJlU2wQ2YJHS0iSrWIJ3Vnx1nJESPV35xUAUCQQCFRtwm\n" +
                    "5/4RVKpCNxnWXjkB4nqAxWC1qxytZsfiwYtHcjdx4dsprv71uwSAB4nfrx8Z5vMO\n" +
                    "8RI1YT6BXzNA5K2N";
            privateKey = privateKey.replace("\n", "");
            String publicKey = new String(Base64.getEncoder().encode(keyPair.getPublic().getEncoded()), "UTF-8");
            System.out.println("私钥:" + privateKey);
            System.out.println("公钥:" + publicKey);
            // RSA加密
            String data = src_msg;//"签名算法可以是NIST标准DSA，使用DSA和SHA-1。 使用SHA-1消息摘要算法的DSA算法可以指定为SHA1withDSA 。 在RSA的情况下，\n" +
                    //"存在对消息多个选择摘要算法，所以签名算法可被指定为，例如， MD2withRSA ， MD5withRSA ，或SHA1withRSA 。 必须指定算法名称，因为没有默认值。";
            String encryptData = encryptByPublicKey(data, getPublicKey(publicKey));
            System.out.println("加密后内容:" + encryptData);
            // RSA解密
            String decryptData = decryptByPrivateKey(encryptData, getPrivateKey(privateKey));
            System.out.println("解密后内容:" + decryptData);

            // RSA签名
            String sign = sign(data, getPrivateKey(privateKey));
            System.out.println("签名内容:" + sign);
            // RSA验签
            boolean result = verify(data, getPublicKey(publicKey), sign);
            System.out.print("验签结果:" + result);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.print("加密解密异常");
        }
    }
}